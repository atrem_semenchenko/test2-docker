<header class="header">
    <hr>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1>Test 2</h1>
            </div>
            <div class="col-md-4">
                <a class="btn btn-primary" href="{{ route('statistic') }}">{{ __('Statistic') }}</a>
                <a class="btn btn-primary" href="{{ route('users-list') }}">{{ __('Users') }}</a>
            </div>
        </div>
    </div>
    <hr>
    <div class="container">
        @if (Session::has('success'))
            <div class="alert alert-info">
                <span>{{ Session::get('success') }}</span>
            </div>
        @endif

        @if (Session::has('error'))
            <div class="alert alert-danger">
                {{ Session::get('error') }}
            </div>
        @endif
    </div>
</header>
