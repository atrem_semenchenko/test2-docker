<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public $timestamps = false;

    protected $fillable = ['firstname', 'lastname', 'gender', 'country_id', 'age', 'created'];

    public function country()
    {
        return $this->hasOne('App\Country', 'id', 'country_id');
    }
}
